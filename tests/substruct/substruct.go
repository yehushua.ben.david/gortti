package substruct
type Strct struct {
	Public string
	private string
}

func (s *Strct) GetPrivate() string{
	return s.private
}

// this method is not reachable with reflection
// maybe with go:linkname
func (s *Strct) readPrivate() string{
	return s.private
}