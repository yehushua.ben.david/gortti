package rtti

import (
	"fmt"
	"reflect"
	"unsafe"
)

type Field struct {
	subFields    map[string]*Field
	subMethods   map[string]*Method
	Index        int
	Name         string
	Type         string
	Owner        interface{}
	Value        interface{}
	ReflectValue reflect.Value
}

func NewObj(obj interface{}) Field {
	return Field{
		Index:        0,
		Name:         "Obj",
		Type:         fmt.Sprintf("%T", obj),
		Owner:        nil,
		Value:        obj,
		ReflectValue: reflect.ValueOf(obj),
	}
}

func (f *Field) Set(value interface{}) {
	f.ReflectValue.Elem().Set(reflect.ValueOf(value))
}
func (f *Field) Field(index int) *Field {
	return &f.Fields()[index]
}

func (f *Field) FieldName(name string) *Field {
	if f.subFields == nil {
		f.Fields()
	}
	return f.subFields[name]
}

func (f *Field) Fields() []Field {
	var fillMap bool
	if f.subFields == nil  {
		fillMap = true
		f.subFields = make(map[string]*Field)
	}
	typeObj := reflect.TypeOf(f.Value)
	valueObj := reflect.ValueOf(f.Value)
	if typeObj.Kind() == reflect.Ptr {
		typeObj = typeObj.Elem()
		valueObj = valueObj.Elem()
	}
	rst := make([]Field, 0)
	if typeObj.Kind() == reflect.Struct {
		for i := 0; i < typeObj.NumField(); i++ {
			privateValue := valueObj.Field(i)
			publicValue := privateValue
			if privateValue.CanAddr() {
				publicValue = reflect.NewAt(privateValue.Type(), unsafe.Pointer(privateValue.UnsafeAddr()))
			}
			var pValue interface{}
			if privateValue.CanAddr() {
				pValue = publicValue.Elem().Interface()
			}
			field := Field{
				ReflectValue: publicValue,
				Value:        pValue,
				Index:        i,
				Owner:        f.Value,
				Name:         typeObj.Field(i).Name,
				Type: func() string {
					PresentedType := fmt.Sprintf("%v", typeObj.Field(i).Type)
					RealType := fmt.Sprintf("%T", pValue)
					if PresentedType != RealType && RealType != "<nil>" {
						return fmt.Sprintf("%s(%s)", PresentedType, RealType)
					}
					return PresentedType
				}(),
			}
			if fillMap {
				f.subFields[field.Name] = &field
			}
			rst = append(rst, field)
		}
	}
	return rst
}

type Method struct {
	Index int
	Owner interface{}
	Name  string
	Type  string
}

func (m *Method) Call(params ...interface{}) []interface{} {
	f := NewObj(m.Owner)
	mth := f.ReflectValue.Method(m.Index)
	vParams := make([]reflect.Value, 0)
	for i := 0; i < len(params); i++ {
		vParams = append(vParams, reflect.ValueOf(params[i]))
	}
	rst := mth.Call(vParams)
	result := make([]interface{}, len(rst))
	for i := 0; i < len(rst); i++ {
		result[i] = rst[i].Interface()
	}
	return result
}

func (f *Field) MethodName(name string) *Method {
	if f.subMethods==nil {
		f.Methods()
	}
	return f.subMethods[name]
}

func (f *Field) Methods() []Method {
	var fillMethod bool
	if  f.subMethods == nil {
		fillMethod = true
		f.subMethods = make(map[string]*Method)
	}
	rm := f.ReflectValue.Type()
	ms := make([]Method, 0)
	for i := 0; i < rm.NumMethod(); i++ {
		method :=  Method{
			Index: i,
			Owner: f.Value,
			Type:  fmt.Sprintf("%v", rm.Method(i).Type),
			Name:  rm.Method(i).Name}
		if fillMethod {
			f.subMethods[method.Name]=&method
		}
		ms = append(ms,method)
	}
	return ms
}

