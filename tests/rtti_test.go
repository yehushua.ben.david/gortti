package tests

import (
	"gortti"
	"gortti/tests/substruct"
	"testing"
)

func Test_setField(t *testing.T) {
	v := struct {
		Str string
		Int int
	}{"Abc", 100}
	dynaObj := rtti.NewObj(&v)
	dynaObj.Fields()[1].Set(dynaObj.Field(1).Value.(int) + 50)
	if v.Int != 150 {
		t.Fatal("set value fail")
	}
}

func Test_setPrivateField(t *testing.T) {
	s := substruct.Strct{
		Public: "pb",
	}
	dynaS := rtti.NewObj(&s)
	str := "private yes! but I got your address"
	dynaS.FieldName("private").Set(str)
	if s.GetPrivate() != str {
		t.Fatal("Can set private field")
	}
}

func Test_callfunc(t *testing.T) {
	s := substruct.Strct{
		Public: "pb",
	}
	dynaS := rtti.NewObj(&s)
	dynaS.FieldName("private").Set("my value")
	rst := dynaS.MethodName("GetPrivate").Call()

	if rst[0].(string) != "my value" {
		t.Fatal("call method fail")
	}
}
